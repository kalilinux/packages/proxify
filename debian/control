Source: proxify
Section: golang
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: ca-certificates,
               debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-armon-go-socks5-dev,
               golang-github-asaskevich-govalidator-dev,
               golang-github-elazarl-goproxy-dev,
               golang-github-hashicorp-golang-lru-dev,
               golang-github-pkg-errors-dev,
               golang-github-rs-xid-dev,
               golang-github-shopify-sarama-dev,
               golang-golang-x-net-dev
Standards-Version: 4.6.2
Vcs-Browser: https://gitlab.com/kalilinux/packages/proxify
Vcs-Git: https://gitlab.com/kalilinux/packages/proxify.git
Homepage: https://github.com/projectdiscovery/proxify
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/projectdiscovery/proxify

Package: proxify
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Swiss Army knife Proxy tool for HTTP/HTTPS traffic capture, manipulation
 This package contains a Swiss Army Knife Proxy for rapid deployments. It
 supports multiple operations such as request/response dump, filtering and
 manipulation via DSL language, upstream HTTP/Socks5 proxy. Additionally a
 replay utility allows to import the dumped traffic (request/responses with
 correct domain name) into burp or any other proxy by simply setting the
 upstream proxy to proxify.
 .
 Features
  * Intercept / Manipulate HTTP/HTTPS & NON-HTTTP traffic
  * Invisible & Thick clients traffic proxy support
  * TLS MITM support with client/server certificates
  * HTTP and SOCKS5 support for upstream proxy
  * Traffic Match/Filter and Replace DSL support
  * Full traffic dump to file (request/responses)
  * Native embedded DNS server
  * Plugin Support to decode specific protocols (e.g XMPP/SMTP/FTP/SSH/)
  * Proxify Traffic replay in Burp
